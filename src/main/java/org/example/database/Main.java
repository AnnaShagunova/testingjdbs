package org.example.database;

import java.sql.*;

public class Main {

    private static final String URL = "jdbc:mysql://localhost:3306/mydbtest";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "root";

    private static final String UPDATE_NAME = "UPDATE users SET name = ? WHERE id = ?";
    private static final String GET_ALL = "SELECT * FROM users";

    public static void main(String[] args) {
        Connection connection;
        PreparedStatement preparedStatement;
        ResultSet resultSet;

        try {
            Driver driver = new com.mysql.cj.jdbc.Driver();
            DriverManager.registerDriver(driver);
        } catch (SQLException e) {
            System.out.println("Неудалось зарегистрировать драйвер");
        }

        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);

            Statement statement = connection.createStatement();
            //Universal
//            statement.execute("INSERT INTO users (name, email, password) values ('Cat', 'cat@mail.ru', '357159');");

            //IUD
//            int res = statement.executeUpdate("UPDATE users SET name = 'Kat' WHERE id = 7");
//            System.out.println(res);

            //SELECT
//            resultSet = statement.executeQuery("SELECT * FROM users");
//            while (resultSet.next()){
//                User user = new User();
//                user.setId(resultSet.getInt("id"));
//                user.setName(resultSet.getString(2));
//                user.setEmail(resultSet.getString("email"));
//                user.setPassword(resultSet.getString(4));
//
//                System.out.println(user);
//            }

            //Транзакции или пакетная обработка
//            statement.addBatch("INSERT INTO users (name, email, password) values ('User1', 'user@mail.ru', '357159');");
//            statement.addBatch("INSERT INTO users (name, email, password) values ('User2', 'user@mail.ru', '357159');");
//            statement.addBatch("INSERT INTO users (name, email, password) values ('User3', 'user@mail.ru', '357159');");
//            statement.executeBatch();
//            statement.clearBatch();


            //PreparedStatement
            preparedStatement = connection.prepareStatement(UPDATE_NAME);
            preparedStatement.setString(1, "User0");
            preparedStatement.setInt(2, 7);
            preparedStatement.execute();

            preparedStatement = connection.prepareStatement(GET_ALL);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                User user = new User();
                user.setId(resultSet.getInt("id"));
                user.setName(resultSet.getString(2));
                user.setEmail(resultSet.getString("email"));
                user.setPassword(resultSet.getString(4));
                System.out.println(user);
            }


            if(!connection.isClosed()){
                System.out.println("Соединение с БД установлено!");
            }

            connection.close();
            if(connection.isClosed()){
                System.out.println("Соединение с БД закрыто!");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
